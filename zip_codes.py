import csv
import shutil as sh
import random as rand

#declare some strings we'll need to read in eventually
city_file_name = '2012_zip_code_use.csv'
infile_name = '2012_individual_use.csv'
outfile_name = 'test.csv'

#open up the list of city files to generate a dictionary mapping cities to zip codes 
city_file = open(city_file_name)
reader = csv.DictReader(city_file , delimiter=',')
city_dict = dict()

for row in reader:
    if row['city'] in city_dict:
        city_dict[row['city']].append(row['zipcode'])
    else:
        city_dict[row['city']] = [row['zipcode']]



#sh.copyfile(infile_name, outfile_name)

#open the output and input files
outfile = open(outfile_name, 'wb')
infile = open(infile_name, 'r')


#initiate the csv reader, will need to switch from a Dictreader to a normal reader to make this
#work the way I want. 
reader = csv.reader(infile, delimiter=',')
fieldnames = reader.next()
fieldnames.append('CorrectedZipCode')

#print fieldnames

zip_index = fieldnames.index('ZipCode')
city_index = fieldnames.index('City')
 

writer = csv.writer(outfile, fieldnames,  delimiter=',', quoting=csv.QUOTE_ALL)

writer.writerow(fieldnames)
#writer.writerow('corrected_zip' : 0)

count = 0
correct_zip_count = 0
wrong_city_count = 0

total_count = 0

#we'll want to put this into a function once it works I think 
for row in reader:
    total_count = total_count + 1
    if row[city_index] in city_dict:
        if row[zip_index] not in city_dict[row[city_index]]:
            #print "zip code: " + row[zip_index] + " in city: " + row[city_index] +  "  does not match, replacing with random selection from" , city_dict[row[city_index]] 
            new_zip = rand.choice(city_dict[row[city_index]])
            row.append(new_zip)
            count = count + 1
            #print row
            writer.writerow(row)
            #print row.append(new_zip)
            #print new_zip
        else:
            correct_zip_count = correct_zip_count + 1
            row.append('no correction')
            writer.writerow(row)
            
    else:
        wrong_city_count  = wrong_city_count + 1
        row.append('city not found')
        writer.writerow(row)


print count
print correct_zip_count
print wrong_city_count
print total_count 
